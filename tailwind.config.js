/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      height:{
        imageH:"650px",
        navbarH:"10%",
        mainH:"90%"
      },
      width:{
        imgW:"450px",
        navLogoW:"30%",
        modalW:"600px",
        labelTextW:"30%",
        inputW:"70%"
      },
      gap:{
        logImgForm:"40px"
      },
      padding:{
        formPad:"80px 82px",
        inputPad:"16px 12px",
        btnPad:"15px 16px",
        navLogoPad:"16px 5px 16px 16px"
      },
      colors:{
        loginTitle:"#303940",
        labelText:'#252C32',
        inputText:"#6E8BB7",
        btnText:"#F6F8F9",
        navText:"#1A2024",
        borderColor:"#E5E9EB",
        modalBtnColor:"#36AD49"
      },
      fontFamily:{
        myFont: 'Inter',
        myFontLogo:'Poppins'
      },
      borderColor:{
        borderC: "#E5E9EB"
      },
      backgroundColor:{
        loginBtn:"#36AD49",
        hoverBtn:"#36b34b",
        navBtn:"#36AD49",
        hoverNavBtn:"#36b34b",
        sectionBg:"#E5E9EB",
        tableBg:"#F4F6FA"
      },
      screens:{
        "md":"873px"
      },
      margin:{
        modalTop:"-40%",
        modalLeft:"30%"
      }
    },
  },
  plugins: [],
}

