import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Login from '../components/Login'
import Home from '../components/Home'
import NotFound from '../components/NotFound'
const Router = () => {
  return (
      <BrowserRouter>
          <Routes>
              <Route path='/' element={<Login />} />
              <Route path='home' element={<Home />} />
              <Route path='*' element={<NotFound/>}/>
          </Routes>
      </BrowserRouter>
  )
}

export default Router