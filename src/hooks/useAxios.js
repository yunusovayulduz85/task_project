import axios from 'axios';
import React, { useEffect, useState } from 'react'
axios.defaults.baseURL = "http://localhost:9000";
const useAxios = (url) => {
    const [res, setRes] = useState([]);
    const [error, setError] = useState("");
    const [loader, setLoader] = useState(true)
    useEffect(() => {
        return async () => {
            try {
                const res = await axios.get(url);
                setRes(res.data);
            } catch (err) {
                setError(err.massage)
            } finally {
                setLoader(false)
            }
        }
    }, [])
    return { res, error, loader,setRes }
}
export default useAxios;