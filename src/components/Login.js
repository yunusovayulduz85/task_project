import React, { useState } from 'react'
import LoginImage from "../assets/LoginImage.png"
import "../index.css"
import { useNavigate } from 'react-router-dom'
const Login = () => {
  const [login,setLogin] = useState()
  const [parol,setParol] = useState()
  const navigation = useNavigate()
  const validate = () => {
    let check = true;
    let errMess = "Please enter the value in ";
    if(login === "" || login === null) {
      check = false;
      errMess +="login"
    }
    if(parol === "" || parol === null) {
      check = false;
      errMess +="parol"
    }
    return check;
  }
  const handleSubmit = (e) => {
    e.preventDefault()
    const userObj ={login,parol}
    if (validate()) {
      fetch("http://localhost:5000/user", {
        method: "POST",
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify(userObj)
      }).then((res) => {
        alert('Registered Successfully!')
        navigation('/home')

      }).catch((err) => {
        alert('Failed : ' + err.massage)
      })
    }
  }
  return (
    <div className='min-h-screen flex items-center justify-center'>
      <div className=' flex items-start bg-white'>
        <div>
          <img className='hidden md:block w-imgW' src={LoginImage} alt='Login Image'/>
        </div>
        <div className='p-formPad flex flex-col justify-between '>
          <h2 className='text-loginTitle font-myFont text-2xl md:text-4xl font-bold mb-20'>Вход в систему</h2>
         <form onSubmit={handleSubmit}>
            <label for="login" className='text-labelText text-base font-semibold font-myFont'>Логин</label> <br/>
            <input
             type='text'
             name='login' 
             placeholder='Введите логин' className='text-inputText text-sm font-myFont font-normal p-inputPad w-full border-borderC border-2 rounded-md bg-white placeholder:text-inputText outline-none mt-1'
             value={login}
             onChange={(e) => setLogin(e.target.value)}
             />
            <br/> <br/>
            <label for='parol' className='text-labelText text-base font-semibold font-myFont'>Пароль</label> <br/>
            <input 
            type='password' 
            name='parol' 
            placeholder='Введите пароль' className='text-inputText text-sm font-myFont font-normal p-inputPad w-full border-borderC border-2 rounded-md bg-white placeholder:text-inputText outline-none mt-1' 
            value={parol}
            onChange={(e) => setParol(e.target.value)}
            />
            <br/>
            <button className='w-full flex justify-center bg-loginBtn p-btnPad rounded-lg text-btnText hover:bg-hoverBtn mt-10'>Войти</button>
         </form>
        </div>
      </div>
    </div>
  )
}

export default Login