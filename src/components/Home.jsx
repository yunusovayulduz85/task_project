import React, { useEffect, useState } from 'react'
import History from "../assets/history.png"
import useAxios from '../hooks/useAxios'
import Edit from "../assets/edit.png"
import Delete from "../assets/delete.png"
import Close from "../assets/close.png"
import axios from 'axios'
import Pagination from './Paginition'
const Home = () => {
  const { res, loader } = useAxios("/users")
  const [user,setUser] = useState([])
  const [phone, setPhone] = useState(false)
  const [showEditDelBtn, setShowEditDelBtn] = useState(false)
  const [clickId, setClickId] = useState()
  const [showModAdUser, setShowModAdUser] = useState(false)
  const [showModEditUser, setShowModEditUser] = useState(false)
  const [newPhone,setNewPhone] = useState()
  //*Yangi user qo'shadigan Modalni inputlarini controlled qilib turadigan stattelarni ochaman:
  const [addDriverId, setAddDriverId] = useState(null)
  const [addId, setAddId] = useState(null)
  const [addFullName, setAddFullName] = useState("")
  const [addPhone, setAddPhone] = useState(null)
  const [addUserType, setAddUserType] = useState("")
  const [addOrders, setAddOrders] = useState("")
  const [addData, setAddData] = useState(null)
  const [userData,setUserData] = useState();
  //* edit modalda default holatdagi  userni ma'lumotlari modalda chiqarib turish uchun 
  const [changeId, setChangeId] = useState(null)
  const [changeDriverId, setChangeDriverId] = useState(null)
  const [changeFullName, setChangeFullName] = useState("")
  const [changePhones, setChangePhones] = useState(null)
  const [changeUserType, setChangeUserType] = useState("")
  const [changeOrders, setChangeOrders] = useState(null)
  const [changeDate, setChangeDate] = useState(null)
  //* edit modalni inputlarini controlled qilish uchun:
  const [editId, setEditId] = useState(null)
  const [editDriverId, setEditDriverId] = useState(null)
  const [editFullName, setEditFullName] = useState("")
  const [editPhones, setEditPhones] = useState(null)
  const [editUserType, setEditUserType] = useState("")
  const [editOrders, setEditOrders] = useState(null)
  const [editDate, setEditDate] = useState(null)
  //! user ma'lumotlarini yangilashda
  // const useStateObj = {
  //   addDriverId,
  //   setAddDriverId,
  //   addFullName,
  //   setAddFullName,
  //   addPhone,
  //   setAddPhone,
  //   addUserType,
  //   setAddUserType,
  //   addOrders,
  //   setAddOrders,
  //   addData,
  //   setAddData
  // }
  // *const [currentPage, setCurrentPage] = useState(1);
  //* const itemsPerPage = 5; // Sayfadaki öğe sayısı
  //* const totalItems = 50;

  //* const handlePageChange = (page) => {
  //*   setCurrentPage(page);
  //* };


  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(5);

  // Get current posts
  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = res.slice(indexOfFirstPost, indexOfLastPost);

  const paginateFront = () => setCurrentPage(currentPage + 1);
  const paginateBack = () => setCurrentPage(currentPage - 1);

  let errMessage = "Please entering the ";
  const validate = () => {
    let check = true
    if( addId === null || addId === ""){
      check = false
      errMessage += "id"
    }
    if( addDriverId === null || addDriverId === ""){
      check = false
      errMessage += "driver id"
    }
    if(addFullName === "" || addFullName === null){
      check = false
      errMessage += "full name"
    }
    if(addPhone === null || addPhone === "" ){
      check = false
      errMessage += "phone"
    }
    if (addUserType === null || addUserType === "" ){
      check = false
      errMessage += "User Type"
    }
    if (addOrders === null || addOrders === "" ){
      check = false
      errMessage += "Orders"
    }
    if (addData === null || addData === "" ){
      check = false
      errMessage += "Date"
    }
    return check;
  }
  const validateEdit = () =>{
    let checkEdit = true
    if (editId === null || editId === "") {
      checkEdit = false
      errMessage += "id"
    }
    if (editDriverId === null || editDriverId === "") {
      checkEdit = false
      errMessage += "driver id"
    }
    if (editFullName === "" || editFullName === null) {
      checkEdit = false
      errMessage += "full name"
    }
    if (editPhones === null || editPhones === "") {
      checkEdit = false
      errMessage += "phone"
    }
    if (editUserType === null || editUserType === "") {
      checkEdit = false
      errMessage += "User Type"
    }
    if (editOrders === null || editOrders === "") {
      checkEdit = false
      errMessage += "Orders"
    }
    if (editDate === null || editDate === "") {
      checkEdit = false
      errMessage += "Date"
    }
    return checkEdit;
  }
  const changePhone = (id) => {
    if(phone){
      setPhone(false)
    }else{
      setPhone(true)
      setClickId(id)
    }
 
  }
  const addNewPhone = (id) => {
    fetch(`http//localhost:9000/users/${id}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        phone: phone.push(newPhone)
      })

    })
    alert('add new phone!')
  }
  const deletePhone = (id) => {
    // fetch(`http//localhost:9000/users/${id}`, {
    //   method: 'PUT',
    //   headers: { 'Content-Type': 'application/json' },
    //   body: JSON.stringify({
    //     phone:null,
    //   })

    // }).then((res) => res.json())
    // res.map((item,index) => {
    //   return (
    //     item.id===id && delete res[index].object.property
    //   )
    // })
    alert('Delete phone!')
  }

  const editDelBtn = (id,userItem) => {
    if (showEditDelBtn) {
      setShowEditDelBtn(false)
    } else {
      setShowEditDelBtn(true)
      setClickId(id)
      setUserData(userItem)
    }
  }

  const addUser = () => {
    setShowModAdUser(true)
    console.log(showModAdUser);
  }
  const userAdded = () => {
    if(validate()){
      setShowModAdUser(false)
          fetch('http://localhost:9000/users', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
              id:addId,
              driverID:addDriverId,
              full_name:addFullName,
              phone:addPhone,
              user_type:addUserType,
              orders:addOrders,
              data:addData
            })
          })
            .then(res => res.json())
            window.location.reload()
            alert('Congratulations!!!')
    }else{
      alert(errMessage)
    }
   
  }

  const editUser = (item) => {
    setShowEditDelBtn(false)
    setShowModEditUser(true)
    setChangeId(item.id)
    setChangeDriverId(item.driverID)
    setChangeFullName(item.full_name)
    setChangePhones(item.phone)
    setChangeUserType(item.user_type)
    setChangeOrders(item.orders)
    setChangeDate(item.data)
  }
  const userEdited = () =>{
    if(validateEdit()){
       setShowModEditUser(false)
      fetch(`http://localhost:9000/users/${clickId}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        id:changeId,
        driverID:changeDriverId,
        full_name:changeFullName,
        phone:changePhones,
        user_type:changeUserType,
        orders:changeOrders,
        data:changeDate
      })
    })
      .then(res => res.json())
      // .then((id) => setUserData(userData.filter((_) => _.id === id)));
      window.location.reload()
       alert("edited!")
    }else{
      alert(errMessage)
    }
   
  }
  const deleteUser = (id) => {
    setShowEditDelBtn(false);
    fetch(`http://localhost:9000/users/${id}`, {
      method: 'DELETE',
    })
      .then(res => res.json())
      .then((id) => setUser(user.filter((_) => _.id === id)));
       window.location.reload()
       alert("removed")
  }
  return (
    <>
     <header className='h-screen relative'>
      <nav className='flex items-center bg-white border-b-2 h-navbarH'>
        <div className='flex items-center w-navLogoW p-navLogoPad justify-between border-r-2'>
          <h3 className='font-myFontLogo font-bold text-2xl'>TASK PROJECT</h3>
          <button className='p-0.5 bg-slate-200 rounded-md'>
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g id="Chevron double left">
                <path id="Vector" fill-rule="evenodd" clip-rule="evenodd" d="M17.707 17.707C17.5195 17.8945 17.2652 17.9998 17 17.9998C16.7349 17.9998 16.4806 17.8945 16.293 17.707L11.293 12.707C11.1056 12.5195 11.0002 12.2652 11.0002 12C11.0002 11.7349 11.1056 11.4805 11.293 11.293L16.293 6.29302C16.3853 6.19751 16.4956 6.12133 16.6176 6.06892C16.7396 6.01651 16.8709 5.98892 17.0036 5.98777C17.1364 5.98662 17.2681 6.01192 17.391 6.0622C17.5139 6.11248 17.6255 6.18673 17.7194 6.28062C17.8133 6.37452 17.8876 6.48617 17.9379 6.60907C17.9881 6.73196 18.0134 6.86364 18.0123 6.99642C18.0111 7.1292 17.9835 7.26042 17.9311 7.38242C17.8787 7.50443 17.8025 7.61477 17.707 7.70702L13.414 12L17.707 16.293C17.8945 16.4805 17.9998 16.7349 17.9998 17C17.9998 17.2652 17.8945 17.5195 17.707 17.707ZM11.707 17.707C11.5195 17.8945 11.2652 17.9998 11 17.9998C10.7349 17.9998 10.4806 17.8945 10.293 17.707L5.29303 12.707C5.10556 12.5195 5.00024 12.2652 5.00024 12C5.00024 11.7349 5.10556 11.4805 5.29303 11.293L10.293 6.29302C10.4816 6.11086 10.7342 6.01007 10.9964 6.01235C11.2586 6.01462 11.5094 6.11979 11.6948 6.3052C11.8803 6.49061 11.9854 6.74142 11.9877 7.00362C11.99 7.26582 11.8892 7.51842 11.707 7.70702L7.41403 12L11.707 16.293C11.8945 16.4805 11.9998 16.7349 11.9998 17C11.9998 17.2652 11.8945 17.5195 11.707 17.707Z" fill="#1A2024" />
              </g>
            </svg>
          </button>


        </div>
        <div className='w-full flex items-center justify-between px-4'>
          <p className='font-myFont text-xl font-semibold text-navText border-r-2 pr-4'>Все водители</p>
          <button onClick={addUser} className='py-2 px-4  bg-navBtn text-white rounded-md hover:bg-hoverNavBtn'>
            + Добавить
          </button>
        </div>
      </nav>
      <main className='h-mainH flex'>
        <aside className='h-full bg-white w-navLogoW'>
          <ul>
            <li className='flex gap-3 px-3 py-3 items-center'>
              <img src={History} alt='общая история заказов' />
              <span className='text-inputText font-myFont text-sm font-medium'> Все заказы</span>

            </li>
          </ul>
        </aside>
        <section className='w-full bg-sectionBg p-4'>
          <div className='bg-white p-4 rounded-md h-full '>
            <table className='w-full max-mainH scroll-y bg-white rounded-md'>
              <tr className='rounded-md'>
                <th className='border-2 p-2 text-center'>№</th>
                <th className='border-2 p-2 text-center'>ID водителя</th>
                <th className='border-2 p-2 text-center'>ФИО водителя</th>
                <th className='border-2 p-2 text-center'>Номер телефона</th>
                <th className='border-2 p-2 text-center'>Все заказы</th>
                <th className='border-2 p-2 text-center'>Тип пользователя</th>
                <th className='border-2 p-2 text-center'>Дата создание</th>
                <th className='border-2 p-2'>
                  <svg className='w-full flex justify-center items-center' width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g id="table_chart">
                      <path id="Vector" d="M8.33333 8.35H12.5V17.5H8.33333V8.35ZM14.1667 17.5H16.6667C17.5833 17.5 18.3333 16.75 18.3333 15.8333V8.33333H14.1667V17.5ZM16.6667 2.5H4.16667C3.25 2.5 2.5 3.25 2.5 4.16667V6.66667H18.3333V4.16667C18.3333 3.25 17.5833 2.5 16.6667 2.5ZM2.5 15.8333C2.5 16.75 3.25 17.5 4.16667 17.5H6.66667V8.33333H2.5V15.8333Z" fill="#36AD49" />
                    </g>
                  </svg>
                </th>
              </tr>
              {
                currentPosts.map((item, index) => {
                  return <>
                    <tr className={(index + 1) % 2 != 0 ? ('bg-tableBg') : ("")}>
                      <td className='border-2 p-2 text-center'>{index + 1}</td>
                      <td className='border-2 p-2 text-center'>{item.driverID}</td>
                      <td className='border-2 p-2 text-center'>{item.full_name}</td>
                      <td className='border-2 p-2 text-center'>
                        <div className='flex justify-between items-center'>
                          {item.phone}
                          <button onClick={() => changePhone(item.id)} className='bg-navBtn text-white px-3 rounded-md flex justify-center items-center hover:bg-hoverNavBtn'>
                            {!phone ? ("+") : ("x")}
                          </button>
                          <button onClick={() => deletePhone(item.id)} className='bg-red-500 text-white px-3 rounded-md flex justify-center items-center hover:bg-red-400'>-</button>
                        </div>
                        {
                        clickId == item.id && phone && <div className='mt-1 flex justify-between'>
                            <input value={newPhone} onChange={(e) => setNewPhone(e.target.value)} className='border-2 px-2 rounded-md placeholder:text-inputText' placeholder='enter the phone..' />
                            <button onClick={() => addNewPhone(item.id)} className='bg-sky-200 px-2 rounded-md'>add</button>
                          </div>
                        }

                      </td>
                      <td className='border-2 p-2 text-center'>{item.orders}</td>
                      <td className='border-2 p-2 text-center'>{item.user_type}</td>
                      <td className='border-2 p-2 text-center'>{item.data}</td>
                      <td className='border-2 p-2 text-center relative'>
                        <button onClick={() => editDelBtn(item.id,item)}>
                          <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g filter="url(#filter0_i_1_8616)">
                              <rect width="48" height="48" fill="#F4F6FA" />
                              <rect x="10.5" y="10.5" width="27" height="27" rx="5.5" fill="white" />
                              <rect x="10.5" y="10.5" width="27" height="27" rx="5.5" stroke="#E5E9EB" />
                              <path d="M20 28.6667C20 29.4 20.6 30 21.3333 30H26.6666C27.4 30 28 29.4 28 28.6667V22C28 21.2667 27.4 20.6667 26.6666 20.6667H21.3333C20.6 20.6667 20 21.2667 20 22V28.6667ZM28 18.6667H26.3333L25.86 18.1933C25.74 18.0733 25.5666 18 25.3933 18H22.6066C22.4333 18 22.26 18.0733 22.14 18.1933L21.6666 18.6667H20C19.6333 18.6667 19.3333 18.9667 19.3333 19.3333C19.3333 19.7 19.6333 20 20 20H28C28.3666 20 28.6666 19.7 28.6666 19.3333C28.6666 18.9667 28.3666 18.6667 28 18.6667Z" fill="#F76659" />
                            </g>
                            <defs>
                              <filter id="filter0_i_1_8616" x="0" y="0" width="48" height="48" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                <feFlood flood-opacity="0" result="BackgroundImageFix" />
                                <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
                                <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                                <feOffset dx="-1" dy="-1" />
                                <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1" />
                                <feColorMatrix type="matrix" values="0 0 0 0 0.898039 0 0 0 0 0.913725 0 0 0 0 0.921569 0 0 0 1 0" />
                                <feBlend mode="normal" in2="shape" result="effect1_innerShadow_1_8616" />
                              </filter>
                            </defs>
                          </svg>
                        </button>
                        {item.id == clickId && showEditDelBtn && <div className='flex flex-col absolute mt-0 bg-white rounded-md z-10  shadow-md right-16'>
                          <button onClick={() => editUser(item)} className='flex items-center justify-center border-b-2 py-2 gap-2 px-10 hover:bg-gray-100 '>
                            <img src={Edit} alt='Edit icon' />
                            <span>
                              Редактировать
                            </span>
                          </button>
                          <button onClick={() => deleteUser(item.id)} className='flex items-center justify-start gap-2 px-6 py-2 hover:bg-gray-100 '>
                            <img src={Delete} />
                            <span>
                              Удалить
                            </span>
                          </button>
                        </div>}
                      </td>

                    </tr>
                  </>
                })
              }

            </table>
           
               <div className=' flex justify-center mt-2'>
              <Pagination
                postsPerPage={postsPerPage}
                totalPosts={res.length}
                paginateBack={paginateBack}
                paginateFront={paginateFront}
                currentPage={currentPage}
              />
            </div>
          </div>
           
        </section>
      </main>

        {
          showModAdUser && <div className='absolute z-10 mt-modalTop ml-modalLeft'>
            <div className='bg-white w-modalW p-4 rounded-md shadow-xl'>
              <div className='flex items-center justify-between pb-2 border-b-2'>
                <h3 className='font-myFont text-xl font-semibold '>Все водители</h3>
                <button onClick={() => setShowModAdUser(false)}>
                  <img src={Close} alt='close modal' />
                </button>
              </div>
              <div>
                <div className='flex items-center justify-between mt-4'>
                  <label for='id' className='w-labelText font-myFont text-sm font-medium text-navText'>ID водителя</label>
                  <input
                    value={addId}
                    onChange={(e) => setAddId(e.target.value)}
                    className='w-inputW rounded-md border-2 border-borderColor py-2 px-4 outline-none'
                    type='number'
                    name='id'
                    placeholder='Введите ид' />
                </div>
                <div className='flex items-center justify-between mt-4'>
                  <label for='idDriver' className='w-labelText font-myFont text-sm font-medium text-navText'>ID водителя</label>
                  <input
                    value={addDriverId}
                    onChange={(e) => setAddDriverId(e.target.value)}
                    className='w-inputW rounded-md border-2 border-borderColor py-2 px-4 outline-none'
                    type='number'
                    name='idDriver'
                    placeholder='Введите города' />
                </div>
                <div className='flex items-center justify-between mt-4'>
                  <label for='fio' className='w-labelText font-myFont text-sm font-medium text-navText'>ФИО водителя</label>
                  <input
                    value={addFullName}
                    onChange={(e) => setAddFullName(e.target.value)}
                    className='w-inputW rounded-md border-2 border-borderColor py-2 px-4 outline-none'
                    type='text'
                    name='fio'
                    placeholder='Введите ФИО водителя' />
                </div>
                <div className='flex items-center justify-between mt-4'>
                  <label for='phone' className='w-labelText font-myFont text-sm font-medium text-navText'>Номер телефона</label>
                  <input
                    value={addPhone}
                    onChange={(e) => setAddPhone(e.target.value)}
                    className='w-inputW rounded-md border-2 border-borderColor py-2 px-4 outline-none'
                    type='number'
                    name='phone'
                    placeholder='Введите Номер телефона' />
                </div>
                <div className='flex items-center justify-between mt-4'>
                  <label for='zakaz' className='w-labelText font-myFont text-sm font-medium text-navText'>Все заказы</label>
                  <input
                    value={addOrders}
                    onChange={(e) => setAddOrders(e.target.value)}
                    type='number'
                    name='zakaz'
                    className='w-inputW rounded-md border-2 border-borderColor py-2 px-4 outline-none' placeholder='Введите Все заказы' />
                </div>
                <div className='flex items-center justify-between mt-4'>
                  <label for='tip' className='w-labelText font-myFont text-sm font-medium text-navText'>Тип пользователя</label>
                  <input
                    value={addUserType}
                    onChange={(e) => setAddUserType(e.target.value)}
                    type='text'
                    name='tip'
                    className='w-inputW rounded-md border-2 border-borderColor py-2 px-4 outline-none' placeholder='Введите Тип пользователя' />
                </div>
                <div className='flex items-center justify-between mt-4'>
                  <label for='date ' className='w-labelText font-myFont text-sm font-medium text-navText'>Дата создание</label>
                  <input
                    value={addData}
                    onChange={(e) => setAddData(e.target.value)}
                    type='date'
                    name='date'
                    className='w-inputW rounded-md border-2 border-borderColor py-2 px-4 outline-none' placeholder='Введите Дата создание' />
                </div>
                <div className='flex items-center justify-end gap-6 mt-4'>
                  <button onClick={() => setShowModAdUser(false)} className='border-2 rounded-md px-6 py-2 text-modalBtnColor hover:bg-green-50'>Отменить</button>
                  <button onClick={userAdded} className='bg-modalBtnColor text-white px-6 py-2 rounded-md hover:bg-green-600'>Сохранить</button>
                </div>
              </div>
            </div>
          </div>
        }

        {
          showModEditUser && <div className='absolute z-10 mt-modalTop ml-modalLeft'>
            <div className='bg-white w-modalW p-4 rounded-md shadow-xl'>
              <div className='flex items-center justify-between pb-2 border-b-2'>
                <h3 className='font-myFont text-xl font-semibold '>Все водители</h3>
                <button onClick={() => setShowModEditUser(false)}>
                  <img src={Close} alt='close modal' />
                </button>
              </div>
              <div>
                <div className='flex items-center justify-between mt-4'>
                  <label for='id' className='w-labelText font-myFont text-sm font-medium text-navText'>Начальное значение : {changeId}</label>
                  <input
                    value={editId}
                    onChange={(e) => setEditId(e.target.value)}
                    className='w-inputW rounded-md border-2 border-borderColor py-2 px-4 outline-none'
                    type='number'
                    name='id'
                    placeholder='Введите ид' />
                </div>
                <div className='flex items-center justify-between mt-4'>
                  <label for='idDriver' className='w-labelText font-myFont text-sm font-medium text-navText'>Начальное значение : {changeDriverId}</label>
                  <input
                    value={editDriverId}
                    onChange={(e) => setEditDriverId(e.target.value)}
                    className='w-inputW rounded-md border-2 border-borderColor py-2 px-4 outline-none'
                    type='number'
                    name='idDriver'
                    placeholder='Введите города' />
                </div>
                <div className='flex items-center justify-between mt-4'>
                  <label for='fio' className='w-labelText font-myFont text-sm font-medium text-navText'>Начальное значение : {changeFullName}</label>
                  <input
                    value={editFullName}
                    onChange={(e) => setEditFullName(e.target.value)}
                    className='w-inputW rounded-md border-2 border-borderColor py-2 px-4 outline-none'
                    type='text'
                    name='fio'
                    placeholder='Введите ФИО водителя' />
                </div>
                <div className='flex items-center justify-between mt-4'>
                  <label for='phone' className='w-labelText font-myFont text-sm font-medium text-navText'>Начальное значение : {changePhones}</label>
                  <input
                    value={editPhones}
                    onChange={(e) => setEditPhones(e.target.value)}
                    className='w-inputW rounded-md border-2 border-borderColor py-2 px-4 outline-none'
                    type='tel'
                    name='phone'
                    placeholder='Введите Номер телефона' />
                </div>
                <div className='flex items-center justify-between mt-4'>
                  <label for='zakaz' className='w-labelText font-myFont text-sm font-medium text-navText'>Начальное значение : {changeUserType}</label>
                  <input
                    value={editUserType}
                    onChange={(e) => setEditUserType(e.target.value)}
                    type='text'
                    name='zakaz'
                    className='w-inputW rounded-md border-2 border-borderColor py-2 px-4 outline-none' placeholder='Введите Все заказы' />
                </div>
                <div className='flex items-center justify-between mt-4'>
                  <label for='tip' className='w-labelText font-myFont text-sm font-medium text-navText'>Начальное значение : {changeOrders}</label>
                  <input
                    value={editOrders}
                    onChange={(e) => setEditOrders(e.target.value)}
                    type='text'
                    name='tip'
                    className='w-inputW rounded-md border-2 border-borderColor py-2 px-4 outline-none' placeholder='Введите Тип пользователя' />
                </div>
                <div className='flex items-center justify-between mt-4'>
                  <label for='date ' className='w-labelText font-myFont text-sm font-medium text-navText'>Начальное значение : {changeDate}</label>
                  <input
                    value={editDate}
                    onChange={(e) => setEditDate(e.target.value)}
                    type='date'
                    name='date'
                    className='w-inputW rounded-md border-2 border-borderColor py-2 px-4 outline-none' placeholder='Введите Дата создание' />
                </div>
                <div className='flex items-center justify-end gap-6 mt-4'>
                  <button onClick={() => setShowModEditUser(false)} className='border-2 rounded-md px-6 py-2 text-modalBtnColor hover:bg-green-50'>Отменить</button>
                  <button onClick={userEdited} className='bg-modalBtnColor text-white px-6 py-2 rounded-md hover:bg-green-600'>Сохранить изменения</button>
                </div>
              </div>
            </div>
          </div>
        }
    </header>
    
    </>
   
  )
}

export default Home