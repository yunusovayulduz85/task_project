import React from 'react'
import CodePen from "../assets/CodePen-404-Page.gif"
const NotFound = () => {
  return (
    <div className='h-screen flex justify-center items-center'>
        <img src={CodePen}/>
    </div>
  )
}

export default NotFound